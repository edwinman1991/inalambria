# FullStack

## Pre-Requisitos

Para el proyecto Angular se requiere instalar los paquetes de NPM:
```
npm install
```
El proyecto inicializa con una base de datos en memoria y posee un set de datos de prueba que seran cargados al momento de iniciar el proyecto.

## Inicializacion

Iniciar RestBackend/RestBackend.Api y en Front la app de Angular.
