interface Proveedor {
    id: number,
    razonSocial: string,
    nit: string,
    telefono: string,
    esGranSuperficie: string
}