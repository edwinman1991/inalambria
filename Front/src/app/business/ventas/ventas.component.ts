import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { BusinessService } from 'app/config/business.service';

@Component({
    selector: 'app-venta',
    templateUrl: './ventas.component.html',
    styleUrls: []
})

export class VentasComponent implements OnInit {

    @ViewChild('confirmModal', { static: false }) private confirmModal;

    productos = [];
    clientes = [];
    proveedores = [];

    factura = {
        detalleFactura: [],
        cliente: {} as Cliente,
        proveedor: {} as Proveedor,
        valorTotal: 0
    };

    canSubmit = false;

    constructor(private backend: BusinessService, private modalService: NgbModal) { }

    ngOnInit() {
        Promise.all([this.backend.getProductos(), this.backend.getClientes(), this.backend.getProveedores()]).then((values:any[])=> {
            this.productos = values[0];
            this.clientes = values[1];
            this.proveedores = values[2];

            this.canSubmit = true;
        });
    }

    onAddItem(f: NgForm) {
        if (f.valid) {

            var toAdd = f.value;
            toAdd.valor = toAdd.cantidad * toAdd.producto.valorUnitario;

            this.deleteIten(toAdd.producto.id);

            this.factura.detalleFactura.push(f.value)
            this.calculateTotal();
            f.resetForm();
        }
    }

    deleteIten(id: number) {
        this.factura.detalleFactura =
            this.factura.detalleFactura.filter(item => item.producto.id !== id);

        this.calculateTotal();
    }

    calculateTotal() {
        if (this.factura.detalleFactura.length <= 0) {
            this.factura.valorTotal = 0;
            return;
        }

        this.factura.valorTotal = this.factura.detalleFactura
            .map(item => item.valor)
            .reduce((prev, next) => prev + next);
    }

    selectCliente(item: any) {
        this.factura.cliente = item;
    }

    selectProveedor(item: any) {
        this.factura.proveedor = item;
    }

    onSubmit(f: NgForm) {
        if (f.valid) {
            this.canSubmit = false;

            var toSubmit = {
                ProveedorId: this.factura.proveedor.id,
                clienteId: this.factura.cliente.id,
                detalles: this.factura.detalleFactura.map(item => ({ productoId: item.producto.id, cantidad: item.cantidad }))
            }

            this.backend.saveFactura(toSubmit).then(x => {
                this.modalService.open(this.confirmModal);
                this.canSubmit = true;
            }).catch(err => {
                alert(err.error[0].errorMessage);
                this.canSubmit = true;
            })
        }
    }

}
