import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { BusinessService } from 'app/config/business.service';

@Component({
  selector: 'app-reporteVentas',
  templateUrl: './reporteVentas.component.html',
  styleUrls: ['./reporteVentas.component.scss']
})
export class ReporteVentasComponent implements OnInit {

  clientes = [];

  itemSeleccionado = {};
  reporte = [];
  canSubmit = false;

  constructor(private backend: BusinessService, private modalService: NgbModal) { }

  ngOnInit() {
    Promise.all([this.backend.getClientes()]).then((values: any[]) => {
      this.clientes = values[0];
      this.canSubmit = true;
    });
  }

  onSubmit(f: NgForm) {
    if (f.valid) {
      var toSubmit = {
        clienteId: (f.value.cliente) ? parseInt(f.value.cliente.id) : null,
        ValorMinimo: (f.value.valorMinimo) ? parseInt(f.value.valorMinimo) : 0,
        anio : (f.value.anio) ? parseInt(f.value.anio) : null,
        granSuperficie: (f.value.granSuperficie == "2" || !f.value.granSuperficie) ? null : (f.value.granSuperficie == "1") ? true : false
      };    
            
      this.canSubmit = false;
      this.backend.getReporte(toSubmit).then((data: []) => {
        this.reporte = data;
        this.canSubmit = true;
      });
    }
  }

  openModal(content, item) {
    this.itemSeleccionado = item;
    this.modalService.open(content, { size: 'lg', ariaLabelledBy: 'modal-basic-title' });
  }

}
