import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { BusinessService } from 'app/config/business.service';

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html'
})
export class ProveedorComponent implements OnInit {

  proveedorSeleccionado = {};
  proveedores = [];
  canSubmit = false;

  constructor(private backend: BusinessService, private modalService: NgbModal) { }

  ngOnInit() {
      this.backend.getProveedores().then((data: []) => {
          this.proveedores = data;
          this.canSubmit = true;
      });
  }

  onSubmit(f: NgForm) {
      if (f.valid) {
          this.canSubmit = false;

          var toSubmit = f.value;

          this.backend.saveProveedor(toSubmit).then(x => {
              this.proveedores.push(x);
              f.resetForm();

              this.canSubmit = true;
          }).catch(err => {
              alert(err.error[0].errorMessage);
              this.canSubmit = true;
          })
      }
  }

  openModal(content, cliente) {
      this.proveedorSeleccionado = cliente;
      this.modalService.open(content);
  }

  onSubmitUpdate(f: NgForm) {
      if (f.valid) {
          this.canSubmit = false;
          this.backend.updateProveedor(f.value).then((x: any) => {
              var index = this.proveedores.findIndex(item => item.id === x.id)
              this.proveedores.splice(index, 1, x)

              this.canSubmit = true;
          }).catch(err => {
              alert(err.error[0].errorMessage);
              this.canSubmit = true;
          })
      }
  }

}
