﻿using RestBackend.Core;
using RestBackend.Core.Models;
using RestBackend.Core.Resources;
using RestBackend.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RestBackend.Services
{
    public class VentaService : IVentaService
    {
        private readonly IUnitOfWork _unitOfWork;

        public VentaService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Venta>> GetSalesByVendor()
        {
            var salesReport = await _unitOfWork.Facturas.GetAllAsync();

            return salesReport
                .GroupBy(x => x.Proveedor)
                .Select(x => new Venta { Facturas = x.ToList(), Proveedor = x.Key });
        }

        public async Task<Venta> GetSalesByVendor(int Id)
        {
            var salesReport = await _unitOfWork.Facturas
                .FindAsync(x => x.ProveedorId == Id, includes: i => i.Proveedor);

            return new Venta { Facturas = salesReport.ToList(), Proveedor = salesReport.FirstOrDefault().Proveedor };
        }

        public async Task<IEnumerable<Venta>> SearchByFilters(VentaQueryResource ventaQuery)
        {
            var salesReport = _unitOfWork.Facturas
                .GetForFilters()
                .Where(f =>
                    (f.Proveedor.EsGranSuperficie == ventaQuery.GranSuperficie || !ventaQuery.GranSuperficie.HasValue) &&
                    (f.ClienteId == ventaQuery.ClienteId || !ventaQuery.ClienteId.HasValue) &&
                    (f.Registro.Year == ventaQuery.Anio || !ventaQuery.Anio.HasValue))
                .ToList();

            return salesReport
                .GroupBy(x => x.Proveedor)
                .Select(x => new Venta { Facturas = x.ToList(), Proveedor = x.Key })
                .Where(t => t.Facturas.Sum(x => x.ValorTotal) >= ventaQuery.ValorMinimo);
        }
    }
}
