﻿using RestBackend.Core;
using RestBackend.Core.Models;
using RestBackend.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RestBackend.Services
{
    public class ProveedorService: IProveedorService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProveedorService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<Proveedor> Create(Proveedor newItem)
        {
            await _unitOfWork.Proveedores.AddAsync(newItem);
            await _unitOfWork.CommitAsync();

            return newItem;
        }

        public async Task Update(Proveedor newItem, Proveedor source)
        {
            source.SetForUpdate(newItem);
            await _unitOfWork.CommitAsync();
        }

        public async Task<IEnumerable<Proveedor>> GetAll()
            => await _unitOfWork.Proveedores
                .GetAllAsync(q => q.OrderBy(s => s.RazonSocial));

        public async Task<Proveedor> GetById(int Id)
            => await _unitOfWork.Proveedores
                .FirstOrDefaultAsync(w => w.Id == Id);

        public async Task<Proveedor> GetByNIT(string Nit)
            => await _unitOfWork.Proveedores
                .FirstOrDefaultAsync(w => w.Nit == Nit);
    }
}
