﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestBackend.Core.Models;
using RestBackend.Core.Resources;
using RestBackend.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestBackend.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VentasController : ControllerBase
    {
        readonly IVentaService _dataService;
        private readonly IMapper _mapper;

        public VentasController(
            IMapper mapper,
            IVentaService dataService)
        {
            _mapper = mapper;
            _dataService = dataService;
        }

        [HttpGet()]
        public async Task<ActionResult<IEnumerable<VentaResource>>> GetByVendors()
        {
            var models = await _dataService.GetSalesByVendor();
            var modelsResources = _mapper.Map<IEnumerable<Venta>, IEnumerable<VentaResource>>(models);

            return Ok(modelsResources);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<VentaResource>>> GetByVendor(int id)
        {
            var models = await _dataService.GetSalesByVendor(id);
            var modelsResources = _mapper.Map<Venta, VentaResource>(models);

            return Ok(modelsResources);
        }

        [HttpPost("search")]
        public async Task<ActionResult<IEnumerable<VentaResource>>> Search(VentaQueryResource ventaQuery)
        {
            var models = await _dataService.SearchByFilters(ventaQuery);
            var modelsResources = _mapper.Map<IEnumerable<Venta>, IEnumerable<VentaResource>>(models);

            return Ok(modelsResources);
        }
    }
}
