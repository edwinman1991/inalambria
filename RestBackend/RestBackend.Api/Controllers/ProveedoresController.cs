﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestBackend.Api.Validators;
using RestBackend.Core.Models;
using RestBackend.Core.Resources;
using RestBackend.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestBackend.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProveedoresController : ControllerBase
    {
        readonly IProveedorService _dataService;
        private readonly IMapper _mapper;

        public ProveedoresController(
            IMapper mapper,
            IProveedorService dataService)
        {
            _mapper = mapper;
            _dataService = dataService;
        }

        [HttpGet()]
        public async Task<ActionResult<IEnumerable<ProveedorResource>>> GetAll()
        {
            var models = await _dataService.GetAll();
            var modelsResources = _mapper.Map<IEnumerable<Proveedor>, IEnumerable<ProveedorResource>>(models);

            return Ok(modelsResources);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProveedorResource>> GetById(int id)
        {
            var model = await _dataService.GetById(id);
            var modelResource = _mapper.Map<Proveedor, ProveedorResource>(model);

            return Ok(modelResource);
        }

        [HttpPost()]
        public async Task<ActionResult<ProveedorResource>> Create([FromBody] NuevoProveedorResource saveResource)
        {
            #region [ Model Validations ]

            var validator = new ProveedorResourceValidator();
            var validationResult = await validator.ValidateAsync(saveResource);

            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors);

            #endregion

            var modelToCreate = _mapper.Map<NuevoProveedorResource, Proveedor>(saveResource);
            var newModel = await _dataService.Create(modelToCreate);

            var model = await _dataService
                .GetById(newModel.Id);

            return Created(nameof(GetById), _mapper.Map<Proveedor, ProveedorResource>(model));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<ClienteResource>> Update(int id, [FromBody] ProveedorResource saveResource)
        {
            var source = await _dataService.GetById(id);
            if (source == default)
                return BadRequest($"El Proveedor {id} no existe.");

            var modelToUpdate = _mapper.Map<ProveedorResource, Proveedor>(saveResource);
            await _dataService.Update(modelToUpdate, source);

            var model = await _dataService.GetById(id);
            return Created(nameof(GetById), _mapper.Map<Proveedor, ProveedorResource>(model));
        }
    }
}
