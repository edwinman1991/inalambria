﻿using FluentValidation;
using RestBackend.Core.Resources;

namespace RestBackend.Api.Validators
{
    public class ProveedorResourceValidator : AbstractValidator<NuevoProveedorResource>
    {
        public ProveedorResourceValidator()
        {

            RuleFor(a => a.RazonSocial)
                .NotEmpty()
                .MinimumLength(2);

            RuleFor(a => a.EsGranSuperficie)
                .NotEmpty();

            RuleFor(a => a.Nit)
                .NotEmpty()
                .MinimumLength(7);

            RuleFor(a => a.Telefono)
                .NotEmpty()
                .MinimumLength(7);
        }
    }

}
