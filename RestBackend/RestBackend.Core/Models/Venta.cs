﻿using RestBackend.Core.Resources;
using System.Collections.Generic;

namespace RestBackend.Core.Models
{
    public class Venta
    {
        public Proveedor Proveedor { get; set; }

        public IList<Factura> Facturas { get; set; }
    }
}
