﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestBackend.Core.Models
{
    public class Proveedor
    {
        public int Id { get; set; }

        public string Nit { get; set; }

        public string RazonSocial { get; set; }

        public string Telefono { get; set; }

        public bool EsGranSuperficie { get; set; } = false;

        public List<Factura> Facturas { get; set; }

        public void SetForUpdate(Proveedor source)
        {
            RazonSocial = source.RazonSocial;
            Telefono = source.Telefono;
            EsGranSuperficie = source.EsGranSuperficie;
        }
    }
}
