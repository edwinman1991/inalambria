using AutoMapper;
using RestBackend.Core.Models;
using RestBackend.Core.Resources;

namespace RestBackend.Core.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Proveedor, ProveedorResource>();
            CreateMap<ProveedorResource, Proveedor>();
            CreateMap<NuevoProveedorResource, Proveedor>();

            CreateMap<Cliente, ClienteResource>();
            CreateMap<ClienteResource, Cliente>();
            CreateMap<NuevoClienteResource, Cliente>();

            CreateMap<Producto, ProductoResource>();
            CreateMap<ProductoResource, Producto>();
            CreateMap<NuevoProductoResource, Producto>();

            CreateMap<FacturaResource, Factura>();
            CreateMap<Factura, FacturaResource>();
            CreateMap<NuevoFacturaResource, Factura>();

            CreateMap<FacturaDetalleResource, FacturaDetalle>();
            CreateMap<FacturaDetalle, FacturaDetalleResource>()
                .ForMember(x => x.Nombre, opt => opt.MapFrom(m => m.Producto.Nombre))
                .ForMember(x => x.ValorUnitario, opt => opt.MapFrom(m => m.Producto.ValorUnitario));

            CreateMap<NuevoFacturaDetalleResource, FacturaDetalle>();

            CreateMap<Factura, ResumenFacturaResource>()
                .ForMember(x => x.Cliente, opt => opt.MapFrom(m => $"{m.Cliente.Nombre} {m.Cliente.Apellido} - {m.Cliente.Cedula}"));

            CreateMap<Venta, VentaResource>()
                .ForMember(x => x.ProveedorId, opt => opt.MapFrom(p => p.Proveedor.Id))
                .ForMember(x => x.ProveedorNit, opt => opt.MapFrom(p => p.Proveedor.Nit))
                .ForMember(x => x.ProveedorRazonSocial, opt => opt.MapFrom(p => p.Proveedor.RazonSocial));

        }
    }
}