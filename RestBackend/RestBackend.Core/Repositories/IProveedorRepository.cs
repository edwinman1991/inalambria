﻿using RestBackend.Core.Models;

namespace RestBackend.Core.Repositories
{
    public interface IProveedorRepository : IRepository<Proveedor>
    {
    }
}
