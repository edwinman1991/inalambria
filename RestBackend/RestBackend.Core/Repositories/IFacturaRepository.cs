﻿using RestBackend.Core.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestBackend.Core.Repositories
{
    public interface IFacturaRepository : IRepository<Factura>
    {
        IQueryable<Factura> GetForFilters();

        Task<IEnumerable<Factura>> GetAllAsync();

        Task<Factura> GetByIdCompleteAsync(int id);
    }
}
