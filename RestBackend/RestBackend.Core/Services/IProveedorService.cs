﻿using RestBackend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RestBackend.Core.Services
{
    public interface IProveedorService
    {
        Task<Proveedor> GetById(int Id);

        Task<Proveedor> GetByNIT(string NIT);

        Task<IEnumerable<Proveedor>> GetAll();

        Task<Proveedor> Create(Proveedor newItem);

        Task Update(Proveedor newItem, Proveedor source);
    }
}
