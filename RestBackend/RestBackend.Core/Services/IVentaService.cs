﻿using RestBackend.Core.Models;
using RestBackend.Core.Resources;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RestBackend.Core.Services
{
    public interface IVentaService
    {
        Task<IEnumerable<Venta>> GetSalesByVendor();

        Task<Venta> GetSalesByVendor(int Id);

        Task<IEnumerable<Venta>> SearchByFilters(VentaQueryResource ventaQuery);
    }
}
