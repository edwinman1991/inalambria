﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RestBackend.Core.Resources
{
    public class VentaResource
    {
        public string ProveedorRazonSocial { get; set; }

        public string ProveedorNit { get; set; }

        public string ProveedorId { get; set; }

        public IList<ResumenFacturaResource> Facturas { get; set; }

        public decimal Total { get => Facturas.Sum(f => f.Total); }

        public int Volumen { get => Facturas.Count; }

    }

    public class ResumenFacturaResource
    {
        public string Numero { get; set; }

        public DateTime Registro { get; set; }

        public string Cliente { get; set; }

        public List<FacturaDetalleResource> Detalles { get; set; }

        public decimal Total { get => Detalles?.Select(x => x.ValorTotal).Sum() ?? 0; }

    }

    public class VentaQueryResource
    {
        public int? ClienteId { get; set; }

        public decimal ValorMinimo { get; set; }

        public bool? GranSuperficie { get; set; }

        public int? Anio { get; set; }
    }
}
