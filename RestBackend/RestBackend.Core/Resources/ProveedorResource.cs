﻿namespace RestBackend.Core.Resources
{
    public class ProveedorResource
    {
        public int Id { get; set; }

        public string Nit { get; set; }

        public string RazonSocial { get; set; }

        public string Telefono { get; set; }

        public bool EsGranSuperficie { get; set; }
    }

    public class NuevoProveedorResource
    {
        public string Nit { get; set; }

        public string RazonSocial { get; set; }

        public string Telefono { get; set; }

        public bool EsGranSuperficie { get; set; }
    }
}
