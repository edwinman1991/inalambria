﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RestBackend.Core.Models;

namespace RestBackend.Data.Configurations
{
    public class ProveedorConfiguration : IEntityTypeConfiguration<Proveedor>
    {
        public void Configure(EntityTypeBuilder<Proveedor> builder)
        {
            builder
                 .HasKey(m => m.Id);

            builder
                .Property(m => m.Id)
                .UseIdentityColumn();

            builder
              .Property(m => m.Nit)
              .IsRequired();

            builder
              .Property(m => m.RazonSocial)
              .IsRequired();

            builder
              .Property(m => m.Telefono)
              .IsRequired();

            builder
                .ToTable("Cliente");
        }
    }
}
