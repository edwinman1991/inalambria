﻿using RestBackend.Core.Models;
using RestBackend.Core.Repositories;

namespace RestBackend.Data.Repositories
{
    public class ProveedorRepository : Repository<Proveedor>, IProveedorRepository
    {
        public ProveedorRepository(RestBackendDbContext context)
            : base(context)
        { }
    }
}
