﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RestBackend.Core.Models;
using System;
using System.Linq;

namespace RestBackend.Data
{
    public class DataGenerator
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new RestBackendDbContext(serviceProvider.GetRequiredService<DbContextOptions<RestBackendDbContext>>()))
            {
                if (context.Clientes.Any())
                    return;

                context.Clientes.AddRange(
                    new Cliente { Id = 1, Apellido = "Mancilla", Nombre = "Edwin", Cedula = "1605637544", Telefono = "3207299734" },
                    new Cliente { Id = 2, Apellido = "Andrade", Nombre = "Carolina", Cedula = "165464654", Telefono = "3217299734" });

                context.Proveedores.AddRange(
                     new Proveedor { Id = 1, Nit = "77028674", Telefono = "60157987987", RazonSocial = "DogChow", EsGranSuperficie = true },
                     new Proveedor { Id = 2, Nit = "88028674", Telefono = "60155587987", RazonSocial = "Chunky", EsGranSuperficie = true },
                     new Proveedor { Id = 3, Nit = "546132564", Telefono = "601555879545", RazonSocial = "Veterinaria Panama" });

                context.Productos.AddRange(
                     new Producto { Id = 1, Nombre = "DogChow 5KG", ValorUnitario = 50000 },
                     new Producto { Id = 2, Nombre = "Chunky 3KG", ValorUnitario = 40000 },
                     new Producto { Id = 3, Nombre = "Consulta general", ValorUnitario = 45000 });

                context.Facturas.AddRange(
                    new Factura
                    {
                        Id = 1,
                        ClienteId = 1,
                        ProveedorId = 3,
                        Numero = "F0001",
                        Registro = DateTime.Now,
                        ValorTotal = 125000,
                        Detalles = new System.Collections.Generic.List<FacturaDetalle> {
                            new FacturaDetalle{ Cantidad = 1, ProductoId = 3, ValorUnitario = 45000 },
                            new FacturaDetalle{ Cantidad = 2, ProductoId = 2, ValorUnitario = 40000 }
                        }
                    },
                    new Factura
                    {
                        Id = 2,
                        ClienteId = 1,
                        ProveedorId = 1,
                        Numero = "F0002",
                        Registro = DateTime.Now.AddYears(-1),
                        ValorTotal = 45000,
                        Detalles = new System.Collections.Generic.List<FacturaDetalle> {
                            new FacturaDetalle{ Cantidad = 1, ProductoId = 1, ValorUnitario = 45000 }
                        }
                    },
                    new Factura
                    {
                        Id = 3,
                        ClienteId = 2,
                        ProveedorId = 2,
                        Numero = "F0003",
                        Registro = DateTime.Now.AddYears(-1),
                        ValorTotal = 45000,
                        Detalles = new System.Collections.Generic.List<FacturaDetalle> {
                            new FacturaDetalle{ Cantidad = 1, ProductoId = 2, ValorUnitario = 45000 }
                        }
                    },
                    new Factura
                    {
                        Id = 4,
                        ClienteId = 2,
                        ProveedorId = 2,
                        Numero = "F0004",
                        Registro = DateTime.Now,
                        ValorTotal = 135000,
                        Detalles = new System.Collections.Generic.List<FacturaDetalle> {
                            new FacturaDetalle{ Cantidad = 3, ProductoId = 2, ValorUnitario = 45000 }
                        }
                    },
                    new Factura
                    {
                        Id = 5,
                        ClienteId = 2,
                        ProveedorId = 2,
                        Numero = "F0005",
                        Registro = DateTime.Now,
                        ValorTotal = 45000,
                        Detalles = new System.Collections.Generic.List<FacturaDetalle> {
                            new FacturaDetalle{ Cantidad = 1, ProductoId = 2, ValorUnitario = 45000 }
                        }
                    },
                    new Factura
                    {
                        Id = 6,
                        ClienteId = 2,
                        ProveedorId = 2,
                        Numero = "F0006",
                        Registro = DateTime.Now.AddDays(-2),
                        ValorTotal = 90000,
                        Detalles = new System.Collections.Generic.List<FacturaDetalle> {
                            new FacturaDetalle{ Cantidad = 2, ProductoId = 2, ValorUnitario = 45000 }
                        }
                    },
                    new Factura
                    {
                        Id = 7,
                        ClienteId = 1,
                        ProveedorId = 3,
                        Numero = "F0007",
                        Registro = DateTime.Now,
                        ValorTotal = 45000,
                        Detalles = new System.Collections.Generic.List<FacturaDetalle> {
                            new FacturaDetalle{ Cantidad = 1, ProductoId = 3, ValorUnitario = 45000 }
                        }
                    },
                    new Factura
                    {
                        Id = 8,
                        ClienteId = 2,
                        ProveedorId = 3,
                        Numero = "F0008",
                        Registro = DateTime.Now.AddYears(-1),
                        ValorTotal = 45000,
                        Detalles = new System.Collections.Generic.List<FacturaDetalle> {
                            new FacturaDetalle{ Cantidad = 1, ProductoId = 3, ValorUnitario = 45000 }
                        }
                    },
                    new Factura
                    {
                        Id = 9,
                        ClienteId = 2,
                        ProveedorId = 2,
                        Numero = "F0009",
                        Registro = DateTime.Now.AddDays(-3),
                        ValorTotal = 400000,
                        Detalles = new System.Collections.Generic.List<FacturaDetalle> {
                            new FacturaDetalle{ Cantidad = 10, ProductoId = 2, ValorUnitario = 40000 }
                        }
                    });

                context.SaveChanges();
            }
        }
    }
}
